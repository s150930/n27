
class Konto {
    constructor() {
        this.Kontonummer
        this.Kontoart
    }
}

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({extended: true}))
app.set('views', 'Training')

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})


//Eine Klasse ist ein Bauplan.
//Ein Objekt ist die Umsetzung auf Grundlage des Bauplans.
//Alle Objekte eines Bauplans haben die selebn Eigenschaften, aber möglicherweise unterschiedliche Eigenschaftswerte.

//Klassendefinition
//===================

class Rechteck{
    constructor(){
        this.laenge
        this.breite
    }
    
}

class Schueler{
    constructor(){
        this.geschlecht
        this.vorname
        this.alter
    }
}

class Fussballer{
    constructor(){
        this.name
        this.verein
        this.position
    }
}

class Auto{
    constructor(){
        this.modell
        this.farbe
        this.preis
    }
}
//Deklaration eines Rechteck-Objekts vom Typ Rechteck
//Deklaration = Bekanntmachung, dass es ein Objekt vom Typ Rechteck geben soll.
//let rechteck =...
//Wichtig: KLasse schreiben wir groß, das objekt klein!
//Instanziierung erkennt man am reservierten Wort "new".
//Instanziierung reserviert Speicherzellen für das soeben deklarierte Objekt.

//...new Rechteck()
//Initialisierung belegt die reservierten Speicherzellen mit ko kreten Eigenschaftswerten.

//rechteck.breite = 3

let rechteck=new Rechteck()
rechteck.breite=3
rechteck.länge=2

console.log("breite :" + rechteck.breite)
console.log("länge :" + rechteck.länge)
console.log(rechteck)


let schueler=new Schueler()
schueler.geschlecht ="w"
schueler.vorname = "Petra"
schueler.alter = 16

let fussballer=new Fussballer()
fussballer.name = "Manuel Neuer"
fussballer.verein = "FC Bayern München"
fussballer.position = "Torwart"

let auto=new Auto()
auto.modell = "Fiat 500"
auto.farbe = "Weiß"
auto.preis = 11.500€



app.get('/klasse-objekt-ejs-trainieren',(req, res, next) => {   

    // ... wird klasse-objekt-ejs-trainieren.ejs gerendert:

    res.render('klasse-objekt-ejs-trainieren', {   
        breite: rechteck.breite, länge: rechteck.länge,
        geschlecht: schueler.geschlecht, vorname: schueler.vorname,
        alter: schueler.alter, name: fussballer.name, verein: fussballer.verein,
        position: fussballer.position, modell
    })
})

