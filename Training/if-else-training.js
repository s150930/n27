/**
 * Hier können Sie IF und ESE trainieren.
 * Am besten dazu den nodemon starten:
 * 
 *  .\node_modules\.bin\nodemon Training\if-else-training.js
 * 
 * und die Ausgabe im Terminal beobachten.
 */
//Die Zahl 1 wird zugewiesen an eine Variable namens x.
 
let x = undefined
let y = 1

if("GW11b".includes("11") ){
    console.log("Die Prüfung ist WAHR. Es werden die Anweisungen im Rumpf von if ausgeführt")
}else{
    console.log("Die Prüfung ist FALSCH. Es werden die Anweisungen im Rumpf von ELSE ausgeführt")
}

//20. if("GW11b".includes("11")) Die prüfung ist wahr. Die Methode "includes" prüft, ob ein String einen anderen String enthält.

//19. if("GW11b".endsWith("b")) Die Prüfung ist wahr. Die Methode "endsWith" prüft, ob ein String mit einem anderen String endet.

//18. if("GW11b".startsWith("GW")) Die Prüfung ist wahr. Die Methode "startsWith" prüft, ob ein String mit einem anderen String beginnt.

//17. if("MA9117.length) Die Prüfung ist wahr. Der Wert von ("MA9117.length") ist 6.

//16. if(undefined) Die Prüfung ist falsch. Wenn ei Objekt nicht existiert ist es undefined.

//15. if(0 < x || x < 10) Logisches oder. Die Prüfung ist wahr, wenn die Linke oder die Rechte oder beide Prüfungen wahr sind.

//14. if(0 < x && x < 11) Logisches und. Die Prüfung ist wahr, wenn beide Prüfungen wahr sind.

//13. if("") Die Prüfung ist falsch.

//12. if(MA9117-*) Die Prüfung ist wahr. Alle nicht leeren Zeichenketten (String) sind wahr.

//11. if(9) Die Prüfung ist wahr. Alle zahlen außer 0 sind wahr.

//10. if(0) Die Prüfung ist falsch. 

//9. if(false) Die Prüfung ist falsch. Der Rumpf von if ist unerreichbarer Code.

//8. if(true) Die Prüfung ist wahr. Der Rumpf von else ist unerreichbarer Code.

//7. if(1 === "1") Prüfung auf Gleichheit des Wertes und Gleichheit des Typs. Die Prüfung ist falsch.

//6. if(0 != 1) Prüfung aug ungleichheit. Die Prüfung ist wahr.

//5. if(0 <= 1) Prüfung auf "keiner gleich". Die Prpüfung ist wahr.

//4. if(0 >= 1) Prüfung auf "größer gleich". Die Prüfung ist falsch.

//3. if(0 < 1) Prüfung auf "kleiner als". Die Prüfung ist wahr.

//2. if(0 > 1) Prüfung auf "größer als". Die Prüfung ist falsch.

//1. if(1 == 1) Prüfung auf Gleichheit des Wertes. Die Prüfung ist wahr.
